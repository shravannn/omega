# omega

**omega** is command-line-utility written in Go which is used to show the proportion of files in a project. 

### Features

* Fast and reliable
* Show the result in a tabulated form
* Export the result in a variety of forms inclusing json and csv.

<!-- todo include images of its action -->

### Set Up

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines 

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? 

* Shravan Asati