/*
The following code is the core of omega - the result of file proportions.

Author: Shravan Asati
Originally Written: 21 April 2021
Last Edited: 21 April 2021
*/

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"math"
)

// stringInSlice checks whether a string is present in a slice of strings.
func stringInSlice(s string, slice []string) bool {
	for _, v := range slice {
		if v == s {
			return true
		}
	}
	return false
}

// handleException prints the error and then exits the program.
func handleException(err error) {
	if err != nil {
		fmt.Println("Error occured:", err)
		os.Exit(-1)
	}
}


// getValidDirsAndFiles returns the directories and files to be evaluated.
func getValidDirsAndFiles() ([]string, []string) {
	files, err := ioutil.ReadDir("./")
	handleException(err)

	ignoreExts, ignoreFiles, ignoreDirs := findIgnores()
	validDirs := []string{}
	validFiles := []string{}


	for _, file := range files {
		if file.IsDir() {
			if stringInSlice(file.Name(), ignoreDirs) {continue}
			validDirs = append(validDirs, file.Name())
		
		} else {
			if stringInSlice(file.Name(), ignoreFiles) {continue}
			separated := strings.Split(file.Name(), ".")
			ext := separated[len(separated) - 1]
			if stringInSlice(ext, ignoreExts) {continue}
			validFiles = append(validFiles, file.Name())
		}

	}

	return validDirs, validFiles
}

func evaluateDir(dir string) []string {
	cwd, err := os.Getwd()
	handleException(err)

	dirPath := filepath.Join(cwd, dir)
	files, e := ioutil.ReadDir(dirPath)
	handleException(e)

	newFiles := []string{} // because files isnt of type string
	for _, f := range files {
		newFiles = append(newFiles, f.Name())
	}

	return newFiles
}

func getExtAndLines(file string) (string, int) {
	byteContent, err := os.ReadFile(file)
	handleException(err)

	content := string(byteContent)
	extension := strings.Split(file, ".")[len(strings.Split(file, ".")) - 1]
	lines := len(strings.Split(content ,"\n"))

	return extension, lines
}

// getResult is the core function, which returns a map of all files and lines.
func getResult() map[string]float64 {
	// * initialising two variables which contain all files and total lines
	allFiles := []string{}
	totalLines := 0
	tempResult := map[string]int{}

	// * getting all valid directories to be evaluated and files
	validDirs, validFiles := getValidDirsAndFiles()
	allFiles = append(allFiles, validFiles...)

	// * getting all files of subdirectories
	for _, dir := range validDirs {
		files := evaluateDir(dir)
		allFiles = append(allFiles, files...)
	}

	// * filling data in the result map, of extension and lines (temporarily)
	for _, f := range allFiles {
		ext, lines := getExtAndLines(f)
		totalLines += lines

		if _, ok := tempResult[ext]; ok {
			tempResult[ext] += lines
		} else {
			tempResult[ext] = lines
		}

	}

	// * the final result, which contains a map of extensions and their percentage
	finalResult := map[string]float64{}
	for k, v := range tempResult {
		finalResult[k] = math.Round((float64(v) / float64(totalLines)) * 100.0)
	}

	return finalResult
}
