package main

import (
	"encoding/json"
	"os"
)

type Result struct { 
	Array map[string]float64 `json:"array"`
} 

func jsonify(result *Result) (string) {
	byteArray, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		panic(err)
	}
	return string(byteArray)
}

func exportJson() {
	data := jsonify(&Result{getResult()})
	f, e := os.Create("omega_result.json")
	handleException(e)

	_, er := f.WriteString(data)
	handleException(er)

	f.Close()
}