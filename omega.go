/*
The following code is responsible for the omega CLI.

Author: Shravan Asati
Originally Written: 21 April 2021
Last Edited: 21 April 2021
*/


package main

import (
	"github.com/thatisuday/commando"
	"fmt"
)

const (
	NAME string = "omega"
	VERSION string = "1.0.0"
)

func main() {
	
	fmt.Println(NAME, VERSION)

	commando.
		SetExecutableName(NAME).
		SetVersion(VERSION).
		SetDescription(fmt.Sprintf("%v is a command line utility which shows project statistics like file percentage etc..", NAME))

	commando.
		Register("show").
		SetShortDescription("Root command").
		SetDescription("root command pro").
		AddFlag("format,f", "Export the results in csv, text or json format.", commando.String, "none").
		SetAction(func(args map[string]commando.ArgValue, flags map[string]commando.FlagValue) {
			fmt.Println("\n\nFile Type \t\t Percentage")
			for k, v := range getResult() {
				fmt.Printf("%v \t\t %v \n", k, v)
			}
		})

	commando.
		Register("export").
		SetShortDescription("ecport").
		SetDescription("ecport").
		AddArgument("format", "the format to export in", "").
		SetAction(func(args map[string]commando.ArgValue, flags map[string]commando.FlagValue){
			fmt.Println("\nExported data to json.")
			exportJson()
		})


	commando.Parse(nil)
}