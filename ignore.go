/*
The following code is for the ignore functionality of omega.

Author: Shravan Asati
Originally Written: 17 April 2021
Last Edited: 18 April 2021
*/

package main

import (
	"os"
	"strings"
)

// findIgnores function returns the extensions, files and directories to be ignored while evaluation as defined in the '.omegaignore' file.
func findIgnores() ([]string, []string, []string) {
	// * opening the .omegaignore file
	file, err := os.ReadFile(`.omegaignore`)
	handleException(err)

	ignoreExts := []string{}
	ignoreDirs := []string{".git", ".svn", ".hg"} // always ignore version control system dirs
	ignoreFiles := []string{".DS_Store", ".omegaignore"} // DS_store for macOS

	// * splitting file content into a slice of lines, for functioning of ignore classifier 
	filecontent := strings.Split(string(file), "\n")
	
	// * iterating over the file lines
	for _, line := range filecontent {
		// * removing leading and trailing whitespaces
		line = strings.TrimSpace(line)

		// * not considering comment or blank line
		if strings.HasPrefix(strings.Trim(line, " "), "#") || line == "" {
			continue
		}
		
		// * determining directories to be ignored
		if strings.HasSuffix(line, "/") {
			dir := strings.Trim(line, "/")
			ignoreDirs = append(ignoreDirs, dir)
		
		// * determining extensions to be ignored
		} else if strings.HasPrefix(line, "*") && len(strings.Split(line, ".")) >= 2 {
			separated := strings.Split(line, ".")
			ext := separated[len(separated) - 1]
			ignoreExts = append(ignoreExts, ext)
		
		// * else the item will be a specific file to be ignored
		} else {
			ignoreFiles = append(ignoreFiles, line)
		}
	}

	return ignoreExts, ignoreFiles, ignoreDirs
}